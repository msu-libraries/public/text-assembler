'''
Handles all web requests for the my searches page
'''
import json
import logging
import os
import tempfile
import zipfile
from itertools import chain
from operator import attrgetter
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.conf import settings
from textassembler_web.utilities import log_error, create_error_message, build_search_info, find_zip_file
from textassembler_web.models import searches, search_users

def mysearches(request):
    '''
    Render the My Searches page
    '''

    # Verify that the user is logged in
    if not request.session.get('userid', False):
        return redirect('/login')

    response = {}
    response["headings"] = ["Date Submitted", "Query", "Progress", "Actions"]

    if "error_message" in request.session:
        response["error_message"] = request.session["error_message"]
        request.session["error_message"] = "" # clear it out so it won't show on refresh


    user_searches = searches.objects.all().filter(userid=request.session['userid'], deleted=False).order_by('-date_submitted')
    addit_searches = searches.objects.all().filter(search_users__userid=request.session['userid'], deleted=False).order_by('-date_submitted')
    all_user_searches = sorted(set(chain(user_searches, addit_searches)), key=attrgetter('date_submitted'), reverse=True)

    for search_obj in all_user_searches:
        search_obj = set_search_info(search_obj, request.session['userid'])

    response["searches"] = all_user_searches
    response["num_months_keep_searches"] = settings.NUM_MONTHS_KEEP_SEARCHES

    return render(request, 'textassembler_web/mysearches.html', response)

def set_search_info(search_obj, logged_in_user):
    '''
    Add additional information to each search result object for the page to use when rendering
    '''

    # Add actions for Download and Delete
    actions = []

    delete = {
        "method": "POST",
        "label": "Delete",
        "action": "delete",
        "class": "btn-danger",
        "args": str(search_obj.search_id)
        }
    download = {
        "method": "POST",
        "label": "Download",
        "action": "download",
        "class": "btn-primary",
        "args": str(search_obj.search_id)
        }

    if search_obj.date_completed != None and search_obj.user_notified > 0:
        actions.append(download)

    # Only allow the submitter to delete the search
    if search_obj.userid == logged_in_user:
        actions.append(delete)

    search_obj.actions = actions

    return build_search_info(search_obj)

def delete_search(request, search_id):
    '''
    Will flag the search as deleted, which the deletion processor will pick up
    and remove from the DB and the storage location
    '''

    error_message = ""
    # Verify that the user is logged in
    if not request.session.get('userid', False):
        return redirect('/login')

    try:
        search_obj = searches.objects.get(search_id=search_id)
        logging.info(f"Marking search as deleted: {search_id}. {search_obj}")

        search_obj.deleted = True
        search_obj.save()

    except Exception as exp: # pylint: disable=broad-except
        error = create_error_message(exp, os.path.basename(__file__))
        log_error(f"Error marking search as deleted:  {search_id}. {error}", json.dumps(dict(request.POST)))

        if settings.DEBUG:
            error_message = error
        else:
            error_message = "An unexpected error has occurred."

    request.session["error_message"] = error_message
    return redirect(mysearches)

def download_search(request, search_id):
    '''
    need to download files from the server for the search
    '''

    # Verify that the user is logged in
    if not request.session.get('userid', False):
        return redirect('/login')

    error_message = ""
    try:
        # make sure the search documents requested are for the user that made the search (HTTP 403)
        error_message = validate_user_access(search_id, str(request.session['userid']))

        # make sure the search file exists (HTTP 404)
        if error_message == "":
            zipfile, error_message = check_file_exists(search_id)

        if error_message == "":
            # download the search zip
            with open(zipfile, 'rb') as flh:
                response = HttpResponse(flh.read(), content_type="application/force-download")
                response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(zipfile)
                request.session["error_message"] = error_message
                return response
    except Exception as exp: # pylint: disable=broad-except
        error = create_error_message(exp, os.path.basename(__file__))
        log_error(f"Error downloading search {search_id}. {error}", json.dumps(dict(request.POST)))

        if settings.DEBUG:
            error_message = error
        else:
            error_message = "An unexpected error has occurred."

    request.session["error_message"] = error_message
    return redirect(mysearches)

def download_selected(request):
    '''
    Compress the selected searches and send them to the user to download
    And handle deletions if requested. TODO rename url route and function
    '''

    # Verify that the user is logged in
    if not request.session.get('userid', False):
        return redirect('/login')

    error_message = ""
    zips = {}
    try:

        if request.POST.get('delete_action'):
            return delete_search(request, request.POST.get('delete_action'))

        # Get the IDs that were requested from the POST parameters
        if request.POST.get('download_action'):
            search_ids = [request.POST.get('download_action')]
        else:
            search_ids = request.POST.getlist('bulkdownload') if 'bulkdownload' in request.POST else []

        for search_id in search_ids:
            # Validate that the user has access to each of the searches they requested
            error_message = validate_user_access(search_id, str(request.session['userid']))

            # make sure the search file exists (HTTP 404)
            if error_message == "":
                zip_file, error_message = check_file_exists(search_id)
                zips[search_id] = zip_file

        # If all validation passed, then compress all the searches for download
        if error_message == "":
            with tempfile.NamedTemporaryFile(dir=settings.STORAGE_LOCATION) as temp_path:
                for search_id in search_ids:
                    with zipfile.ZipFile(temp_path.name, 'a', zipfile.ZIP_DEFLATED) as zipf:
                        zipf.write(zips[search_id], os.path.basename(zips[search_id]))

                # Download the completed search zip
                with open(temp_path.name, 'rb') as flh:
                    response = HttpResponse(flh.read(), content_type="application/force-download")
                    response['Content-Disposition'] = 'attachment; filename=bulkdownload.zip'
                    request.session["error_message"] = error_message
                    return response

        # If the validation did not pass, return the errors to the user
        request.session["error_message"] = error_message
        return redirect(mysearches)

    except Exception as exp: # pylint: disable=broad-except
        error = create_error_message(exp, os.path.basename(__file__))
        log_error(f"Error downloading search {search_id}. {error}", json.dumps(dict(request.POST)))

        if settings.DEBUG:
            error_message = error
        else:
            error_message = "An unexpected error has occurred."

    request.session["error_message"] = error_message
    return redirect(mysearches)

def validate_user_access(search_id, user_id):
    '''
    Checks if the user has access to the given search and returns any error message
    '''
    error_message = ""
    search_obj = searches.objects.filter(search_id=search_id)

    if len(search_obj) == 1:
        search_obj = search_obj[0]
    else:
        error_message = \
            "The search record could not be located on the server. please contact technical support."
    if search_obj.userid != user_id:
        error_message = "You do not have permissions to download searches other than ones you requested."

    return error_message

def check_file_exists(search_id):
    '''
    Ensure the requested search's file exists on the filesystem
    and returns the zipfile path and any error message
    '''
    error_message = ""
    zipfile = find_zip_file(search_id)

    if zipfile is None or not os.path.exists(zipfile) or not os.access(zipfile, os.R_OK):
        error_message = \
            "The search results can not be located on the server. please contact technical support."

    return zipfile, error_message
