#!/usr/bin/env python3

####################################################################################
# One-off script to run a bunch of search queries against the API
# To determine the number of results they would all return.
# This is better than having to go through the web site one by one
# when there are a large number of searches you want to do this for.
#
# Input:
#   A CSV file containing:
#       query, filter_name|filter_value, filter_name|filter_value, ...
# Output:
#   A CSV file containing:
#       query, num_results, filter_name|filter_value, filter_name|filter_value, ...
####################################################################################

import os
import sys
import csv
import time
import argparse
from django.utils import timezone
from django.core.wsgi import get_wsgi_application
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'textassembler.settings')
PATH = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
if PATH not in sys.path:
    sys.path.append(PATH)
application = get_wsgi_application()
from textassembler_web.ln_api import LNAPI

APPLY_TO_ALL = {"Language":["English"]}

API = LNAPI()

def parse_file(filename):
    searches = []
    print(f"===== Starting parsing of input file: {filename} ====")
    with open(filename, 'r') as csvfile:
        lines = csvfile.readlines()
        for row in lines:
            tmp = {"query": row.replace('\n',''), "filters": APPLY_TO_ALL}
            searches.append(tmp)
    print(f"==== Identified {len(searches)} searches to process. ====")
    return searches

def add_results_to_output(search, filename):
    with open(filename, 'a') as csvfile:
        csvfile.write(str(search['num_results']) + "," + search['query'] + "\n")

def wait_for_search():
    '''
    Will wait for an open search window before returning, checking periodically
    '''
    try:
        avail_time = API.check_when_available('search')
        if avail_time > timezone.now():
            print(f"---- No searches remaining. Must wait until {avail_time.strftime('%c')} until next window is available. ----")
            while API.check_when_available('search') > timezone.now():
                time.sleep(30)
            print("---- Resuming processing ----")
    except Exception as ex:
        print(f"Error waiting for search window. {ex}")
        exit(1)


def perform_searches(searches, output):
    print(f"==== Starting processing of {len(searches)} searches. ====")

    idx = 1
    for search in searches:
        print(f"++++ {idx}/{len(searches)} -- Running search for: {search['query']}")
        wait_for_search()
        results = API.search(search['query'], search['filters'])
        if '@odata.count' in results:
            search['num_results'] = results['@odata.count']
            add_results_to_output(search, output)
            print(f"++++ Found {search['num_results']} results.")
        else:
            print(f"Error returned from LexisNexis: {'[Not Set]' if not results['error_message'] else results['error_message']}")
            exit(1) # right now we just want to stop instead of doing a bunch of queries with a lot of errors
        idx = idx + 1

    print(f"==== Completed processing of {len(searches)} searches. ====")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Bulk search the LexisNexis API to get result counts')
    parser.add_argument('-i', '--input', help='CSV file with the query and filters',required=True)
    parser.add_argument('-o', '--output', help='CSV file with the query, result count and filters',required=True)
    args = vars(parser.parse_args())

    searches = parse_file(args['input'])
    perform_searches(searches, args['output'])
