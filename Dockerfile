FROM python:3.8

ENV PYTHONUNBUFFERED=1

RUN set -x && \
  echo mail > /etc/hostname && \
  echo "postfix postfix/main_mailer_type string Internet site" >> preseed.txt && \
  echo "postfix postfix/mailname string textassembler.lib.msu.edu" >> preseed.txt && \
  debconf-set-selections preseed.txt && rm preseed.txt

RUN DEBIAN_FRONTEND=noninteractive && \
    apt-get update && apt-get -y install vim postfix

WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
COPY main.cf /etc/postfix/main.cf

EXPOSE 8000

CMD bash -c "service postfix start && sleep 5 && python manage.py migrate && python manage.py collectstatic && gunicorn textassembler.wsgi -b 0.0.0.0:8000"
