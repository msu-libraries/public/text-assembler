import traceback
import os
import json
import time
import logging
import csv
import zipfile
from datetime import datetime
from django.conf import settings
from django.db.utils import OperationalError
from django.db.models import F
from django.apps import apps
from textassembler_web.utilities import log_error, send_user_notification
from textassembler_web.ln_api import LNAPI
from textassembler_processor.output_utilities import prepare_for_csv, prepare_for_json, prepare_for_text, prepare_for_xhtml

def wait_for_download(terminate_event, retry_count = 0):
    '''
    Will wait for an open download window before returning, checking periodically
    args:
        terminate_event(Event): indicator that we should stop waiting and just return
        retry_count(int): Used to track the number of failures we've had already
    returns:
        (bool): Indicating if the function was successful or not
    '''
    status = True
    try:
        api = LNAPI()
        avail_time = api.check_when_available('download')
        seconds_to_wait = (avail_time - datetime.now()).seconds
        if avail_time > datetime.now():
            logging.info(f"(Retriever) No downloads remaining. Must wait until {avail_time.strftime('%c')} until next available download window is available.")
            # Check if we can download every 10 seconds instead of waiting the full wait_time to
            # be able to handle sig_term triggering (i.e. we don't want to sleep for an hour before
            # a kill command is processed)
            while avail_time > datetime.now() and not terminate_event.is_set():
                seconds_to_wait = (avail_time - datetime.now()).seconds
                if seconds_to_wait > 5:
                    time.sleep(5)
                else:
                    time.sleep(seconds_to_wait)
                avail_time = api.check_when_available('download')
            logging.info("(Retriever) Resuming processing")
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            status = wait_for_download(terminate_event, retry_count + 1)
        else:
            log_error(f"Stopping queue processor. {traceback.format_exc()}")
            status = False
    return status

def get_search_by_id(search_id, retry_count = 0):
    '''
    Get the search from the database
    args:
        search_id(int): ID to pull the record for
        retry_count(int): Used to track the number of failures we've had already
    returns:
        (Search/None): database record for the next search to process, will
            be None if there is nothing to search
    '''
    search = None
    try:
        # check that there are items in the queue to process
        searches = apps.get_model('textassembler_web', 'searches')
        search = searches.objects.get(search_id=search_id)
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            search = get_search_by_id(search_id, retry_count + 1)
        else:
            log_error(f"Failed to get_search_by_id (search id: {search_id}) after {settings.NUM_RETRIES} attempts. {traceback.format_exc()}")
    return search

def get_next_search(retry_count = 0):
    '''
    Get the next search queue from the database
        retry_count(int): Used to track the number of failures we've had already
    returns:
        (Search/None): database record for the next search to process, will
            be None if there is nothing to search
    '''
    search = None
    try:
        # check that there are items in the queue to process
        searches = apps.get_model('textassembler_web', 'searches')
        queue = searches.objects.filter(date_completed__isnull=True, failed_date__isnull=True,
            deleted=False, on_hold_date__isnull=True,
            skip_value_retriever__lt=F('num_results_in_search')).order_by('update_date')
        if queue:
            search = queue[0]
        else:
            search = 0
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            search = get_next_search(retry_count + 1)
        else:
            log_error(f"Failed to get the next search in the queue after {settings.NUM_RETIRES} attempts. {traceback.format_exc()}")
    return search

def get_next_search_to_finalize(retry_count = 0):
    '''
    Get the next search to finalize from the database
        retry_count(int): Used to track the number of failures we've had already
    returns:
        (Search/None): database record for the next search to process, will
            be None if there is nothing to search
    '''
    search = None
    try:
        # check that there are items in the queue to process
        searches = apps.get_model('textassembler_web', 'searches')
        queue = searches.objects.filter(date_completed__isnull=False,
            deleted=False, user_notified=False).order_by('update_date')
        if queue:
            search = queue[0]
        else:
            search = 0
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            search = get_next_search_to_finalize(retry_count + 1)
        else:
            log_error(f"(Finalizer) Failed to get the next search in the queue after {settings.NUM_RETIRES} attempts. {traceback.format_exc()}")
    return search

def get_search_results(search, force_extract = False, retry_count = 0):
    '''
    Retrieve the next batch of results from the API
    args:
        search(Search): record to use for building the query
        retry_count(int): indicating the number of failures this function has had already
    returns:
        (dict/None): results from the API for the search
    '''
    valid_results = None
    try:
        api = LNAPI()
        set_filters = get_search_filters(search)
        logging.debug(f"(Retriever) Retrieving results for {search.search_id} with a skip value of {search.skip_value_retriever}, retry count: {retry_count}, force extract: {force_extract}")
        # Fall back to extract only when we've failed more than once
        if search.extract_only or force_extract:
            results = api.search(search.query,
                set_filters, "" if search.sort_order is None else search.sort_order.sort_value,
                settings.LN_DOWNLOAD_PER_CALL,
                search.skip_value_retriever)
        else:
            results = api.download(search.query,
                set_filters, "" if search.sort_order is None else search.sort_order.sort_value,
                settings.LN_DOWNLOAD_PER_CALL,
                search.skip_value_retriever)

        # Check for errors from the API
        ## Check for throttle limit errors
        if "response_code" in results and results["response_code"] == 429 and "error_message" in results:
            time.sleep(60 * 5) # TODO remove this when we figure out issue with the rate limit reset time
            raise Exception((f"An error occurred processing search id: {search.search_id}. "
                           f"Misconfigured throttle limits. {results['error_message']}")) from None

        ## Check for gateway timeout or any other server error
        elif "response_code" in results and results["response_code"] >= 500:
            raise Exception(f"An API server error occured occured processing search {search.search_id}. {results['response_code']}") from None
        ## Check for any other errors
        elif "error_message" in results:
            raise Exception(f"An error occurred processing search id: {search.search_id}. {results['error_message']}", results) from None
        ## Otherwise, we have valid results
        else:
            valid_results = results

    except Exception as exp: # pylint: disable=broad-except
        if retry_count <= settings.NUM_RETRIES:
            logging.error(f"Failed to get results for search. {exp}. {traceback.format_exc()}")
            time.sleep(30)
            results = get_search_results(search, force_extract, retry_count + 1)
        else:
            log_error(f"Failed to get results for search: {exp}. {traceback.format_exc()}")
    return valid_results

def get_search_filters(search):
    '''
    Set the filters for the search to use in the API call
    args:
        search(Search): the search record to get filters for
    returns:
        (Filters): filters to apply for the provided search
    '''
    filters = apps.get_model('textassembler_web', 'filters')
    all_filters = filters.objects.filter(search_id=search)
    set_filters = {}
    for fltr in all_filters:
        if fltr.filter_name not in set_filters:
            set_filters[fltr.filter_name] = []
        set_filters[fltr.filter_name].append(fltr.filter_value)
    return set_filters

def set_start_time(search, retry_count = 0):
    '''
    Set the start time for the search to now
    args:
        search(Search): the object to update the start time for
        retry_count(int): the number of failures that have occured
    returns:
        (bool): If the function was a success or not
    '''
    status = True

    if not search.date_started:
        search.date_started = datetime.now()
        try:
            search.save(update_fields=["date_started"])
        except OperationalError as ex:
            if retry_count <= settings.NUM_RETRIES:
                logging.error(traceback.format_exc())
                time.sleep(30)
                status = set_start_time(search, retry_count + 1)
            else:
                log_error(f"Stopping queue processor. {traceback.format_exc()}")
                status = False
    return status

def update_run_time(search, start_time, retry_count = 0):
    '''
    Set the start time for the search to the given time
    args:
        search(Search): the record to update
        start_time(Datetime): time to set on the record in database
        retry_count(int): the number of failures that have occured
    returns:
        (bool): If the function was a success or not
    '''
    status = True

    search.update_date = datetime.now()
    search.run_time_seconds = search.run_time_seconds + int(round(time.time() - start_time, 0))

    try:
        search.save(update_fields=["update_date", "run_time_seconds"])
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            status = update_run_time(search, start_time, retry_count + 1)
        else:
            log_error(f"Stopping queue processor. {traceback.format_exc()}")
            status = False

    return status

def update_retriever_skip_value(search, retry_count = 0):
    '''
    Update the skip value for the retriever process
    args:
        search(Search): the record to update
        retry_count(int): the number of failures that have occured
    returns:
        (bool): If the function was a success or not
    '''
    status = True

    search.update_date = datetime.now()
    search.skip_value_retriever = search.skip_value_retriever + settings.LN_DOWNLOAD_PER_CALL
    try:
        search.save(update_fields=["update_date", "skip_value_retriever"])
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            status = update_retriever_skip_value(search, retry_count + 1)
        else:
            log_error(f"Stopping queue processor. {traceback.format_exc()}")
            status = False

    return status

def sync_all_skip_values(retry_count = 0):
    '''
    Reset the retriever's skip value to match the saver's skip value
    for all searches in the queue.
    args:
        retry_count(int): the number of failures that have occured
    returns:
        (bool): If the function was a success or not
    '''
    status = True

    try:
        searches = apps.get_model('textassembler_web', 'searches')
        queue = searches.objects.filter(date_completed__isnull=True, failed_date__isnull=True,
                                        deleted=False, on_hold_date__isnull=True,
                                        skip_value__lt=F('num_results_in_search'))
        for search in queue:
            logging.debug(f"(Main) Setting skip_value_retriever from {search.skip_value_retriever} " \
                          f"to {search.skip_value} for search {search.search_id}")
            search.update_date = datetime.now()
            search.skip_value_retriever = search.skip_value
            search.save(update_fields=["update_date", "skip_value_retriever"])
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            status = sync_all_skip_values(retry_count + 1)
        else:
            log_error(f"Stopping queue processor from sync_all_skip_values. {traceback.format_exc()}")

    return status

def sync_skip_value(search, retry_count = 0):
    '''
    Reset the retriever's skip value to match the saver's skip value
    args:
        search(Search): the record to update
        retry_count(int): the number of failures that have occured
    returns:
        (bool): If the function was a success or not
    '''
    status = True

    search.update_date = datetime.now()
    logging.debug(f"(Main) Setting skip_value_retriever from {search.skip_value_retriever} to {search.skip_value} for search {search.search_id}")
    search.skip_value_retriever = search.skip_value
    try:
        search.save(update_fields=["update_date", "skip_value_retriever"])
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            status = sync_skip_value(search, retry_count + 1)
        else:
            log_error(f"Stopping queue processor. {traceback.format_exc()}")

    return status

def reset_over_max_skip_values(retry_count = 0):
    '''
    Reset the skip values to match the number of results in the search
    for any searches where the skip is larger
    args:
        retry_count(int): the number of failures that have occured
    returns:
        (bool): If the function was a success or not
    '''
    status = True

    try:
        searches = apps.get_model('textassembler_web', 'searches')
        queue = searches.objects.filter(date_completed__isnull=True, failed_date__isnull=True,
                                        deleted=False, on_hold_date__isnull=True,
                                        skip_value__gt=F('num_results_downloaded'),
                                        skip_value_retriever__gt=F('num_results_downloaded'))
        for search in queue:
            logging.info(f"(Main) Identified search (ID={search.search_id}) where the skip " \
                         f"value is greater than the number of results in the search. " \
                         f"Reseting the skip values to the number of results downloaded. " \
                         f"Setting skip_retriever from {search.skip_value_retriever} " \
                         f"to {search.num_results_downloaded} and skip_value from " \
                         f"{search.skip_value} to {search.num_results_downloaded}")
            search.update_date = datetime.now()
            search.skip_value_retriever = search.num_results_downloaded
            search.skip_value = search.num_results_downloaded
            search.save(update_fields=["update_date", "skip_value_retriever", "skip_value"])
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            status = reset_over_max_skip_values(retry_count + 1)
        else:
            log_error(f"Stopping queue processor from reset_over_max_skip_values. {traceback.format_exc()}")

    return status

def save_results(search, results):
    '''
    Will save the provided results to the server
    args:
        search(Search): the search the results are for
        results(dict): the results from the API
    returns:
        (bool, int, int):
            status: if the save was successful
            saved: number of results saved
            total: number of results in the search
    '''
    return save_aggregate(search, results) if search.aggregate_results else save_individual(search, results)

def save_aggregate(search, results):
    '''
    Will save the provided results to the server in a single aggregated file
    args:
        search(Search): the search the results are for
        results(dict): the results from the API
    returns:
        (bool, int, int):
            status: if the save was successful
            saved: number of results saved
            total: number of results in the search
    '''
    status = True
    saved = len(results["value"])
    total = results['@odata.count']

    # Get the content prepared in the correct format
    (content, extension) = prepare_content(
        search.output_format, results, is_aggregate=True, is_extract_only=search.extract_only)

    if not content:
        log_error(f"WARNING: Could not parse result value from search for ID: {search.search_id}.", json.dumps(results))
        saved = 0
    else:
        # Determine file save location
        save_location = os.path.join(settings.STORAGE_LOCATION, str(search.search_id))
        file_name = f"{search.search_id}{extension}"
        full_path = os.path.join(save_location, file_name)

        # Write the content to the save location
        status = write_content(save_location, full_path, content, search.output_format)

    return (status, saved, total)

def write_content(save_location, full_path, content, output_format, retry_count = 0):
    '''
    Write the provided content to the save location in the format specified
    args:
        save_location(str): base location on the server to save the results
        full_path(str): full path on the server to where the content should be written
        content(dict): resultsfrom the API to save
        output_format(str): valid result format to convert the content to
        retry_count(int): number of times this function has failed already
    return:
        (bool): If the function was a success or not
    '''
    status = True
    try:
        if not os.path.exists(save_location):
            os.makedirs(save_location)
        if output_format == "csv":
            writeheader = not os.path.exists(full_path)
            with open(full_path,"a+") as fh:
                writer = csv.DictWriter(fh, fieldnames=content[0].keys())
                if writeheader:
                    writer.writeheader()
                for row in content:
                    writer.writerow(row)
        else:
            open(full_path,"a+").write(content + "\n")
    except (OSError, IOError) as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(10)
            status = write_content(save_location, full_path, content, output_format, retry_count + 1)
        else:
            log_error(f"Stopping queue processor. {traceback.format_exc()}")
            status = False
    return status

def prepare_content(output_format, results, is_aggregate = False, is_extract_only = False):
    '''
    Prepare the raw API resuts in the format specified
    args:
        output_format(str): format to conver the results to
        results(dict): results from the API
        is_aggregate(bool): if the results should be aggregated in a single file
        is_extract_only (bool): If the results contain only extracts instead of full-text
    return:
        (list/str, str): the parsed content and the file extension to use
    '''
    content = None
    extension = None

    if is_extract_only:
        # For simplicity, only allowing JSON for this option since this if for debugging
        # and not a feature available for users via the UI
        content = prepare_for_json(results["value"]) if is_aggregate else prepare_for_json(results)
        extension = ".json"
    if output_format == "csv" and is_aggregate:
        for result in results["value"]:
            if not content:
                content = []
            content.append(prepare_for_csv(result))
        extension = ".csv"
    elif output_format == "json":
        content = prepare_for_json(results["value"]) if is_aggregate else prepare_for_json(results)
        extension = ".json"
    elif output_format == "text" and not is_aggregate:
        content = prepare_for_text(results)
        extension = ".txt"
    elif output_format == "xhtml" and not is_aggregate:
        content = prepare_for_xhtml(results)
        extension = ".xhtml"

    return (content, extension)

def save_individual(search, results, retry_count = 0):
    '''
    Will save the provided results to the server in individual files
    args:
        search(Search): the search the results are for
        results(dict): the results from the API
        retry_count(int): the number of failures this function has had already
    returns:
        (bool, int, int):
            status: if the save was successful
            saved: number of results saved
            total: number of results in the search
    '''
    status = True
    saved = len(results["value"])
    total = results['@odata.count']
    zipf = None

    # Set the path to save the result in
    save_location = os.path.join(settings.STORAGE_LOCATION, str(search.search_id))
    zip_name = settings.APP_NAME.replace(" ", "") + "_" + search.date_submitted.strftime("%Y%m%d_%H%M%S") + ".zip"
    full_zip_path = os.path.join(save_location, zip_name)

    # Make the result directory if it does not exist
    if not os.path.exists(save_location):
        try:
            os.mkdir(save_location)
        except Exception as ex:
            log_error(f"Failed to create directory {save_location} for search {search.search_id}. {traceback.format_exc()}")
            status = False

    # Open the zipfile
    try:
        #logging.debug(f"(Saver) Preparing to open {full_zip_path}")
        zipf =  zipfile.ZipFile(full_zip_path, 'a', zipfile.ZIP_DEFLATED)
        #logging.debug(f"(Saver) File handle created for {full_zip_path}")
    except Exception as ex:
        log_error(f"Failed to open {full_zip_path} for search {search.search_id}. {traceback.format_exc()}")
        status = False
        saved = 0
        return (status, saved, total)

    # Process each result
    for result in results["value"]:
        # Get the content prepared in the correct format
        (content, extension) = prepare_content(
            search.output_format, result, is_aggregate=False, is_extract_only=search.extract_only)

        # If there is no content, log and continue
        if not content:
            log_error(f"WARNING: Could not parse result value from search for ID: {search.search_id}.", json.dumps(result))
            saved = saved - 1
            continue

        # Determine sub-directory within the zip to save to
        if search.last_save_dir:
            if search.last_save_dir_size >= settings.MAX_FILES_PER_DIR:
                search.last_save_dir = str(int(search.last_save_dir) + 1)
                search.last_save_dir_size = 0
        else:
            search.last_save_dir = "1"

        # Determine file name
        file_name = result["ResultId"].replace("urn:contentItem:", "")
        unique_timestamp = datetime.now().strftime('%d%H%M%S%f')
        file_name = os.path.join(search.last_save_dir, f"{unique_timestamp}_{file_name}{extension}")

        # Move the file into the zip
        if status:
            try:
                #logging.debug(f"(Saver) Preparing to write {file_name}")
                zipf.writestr(file_name, content)
                #logging.debug(f"(Saver) File write complete for {file_name}")
            except Exception as ex:
                log_error(f"Failed to add {file_name} to {full_zip_path} for search {search.search_id}. {traceback.format_exc()}")
                status = False

    #logging.debug(f"(Saver) Closing zipfile for search {search.search_id}")
    zipf.close()
    #logging.debug(f"(Saver) Done closing zipfile for search {search.search_id}")

    return (status, saved, total)

def finalize_search(search, retry_count = 0):
    '''
    Finalize the given search by compressing the aggregated results (if needed)
    and notifying the user.
    args:
        search(Search): record to finalize
        retry_count(int): the number of times this function has failed already
    returns:
        (bool): if the function was successful or not
    '''
    status = True

    try:
        # Do nothing if the search is not complete or the user has already been notified
        if search.date_completed is None or search.user_notified:
            return status

        # Compress the search results, if aggregate (individual are compressed as downloaded)
        if search.aggregate_results:
            status = compress_aggregate_results(search)
        if not status:
            return status

        # Set the appropriate field values to finalize
        search.update_date = datetime.now()
        search.user_notified = True

        # save the search record (we do this before sending the notification to prevent
        # multiple emails being sent in case of db update failure)
        search.save(
            update_fields= ["update_date", "user_notified"])

        # Notify the user of completion
        search_users = apps.get_model('textassembler_web', 'search_users')
        if settings.NOTIF_EMAIL_DOMAIN:
            notif_users = [search.userid] + [x.userid for x in search_users.objects.filter(search_id=search) if x.userid]
            send_user_notification(notif_users, search.query, search.date_submitted, search.num_results_downloaded)
            # TODO check if we need to catch any errors
            logging.info(f"(Saver) Notified {', '.join(notif_users)} of completed search {search.search_id}")

    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            status = finalize_search(search, saved, total, retry_count + 1)
        else:
            log_error(f"Failed to finalize search {search.search_id}. {traceback.format_exc()}")
            status = False
    return status

def compress_aggregate_results(search):
    '''
    Given an aggregated search, compress the result file
    args:
        search(Search): search to compress results for
    return:
        (bool): if the function was successful or not
    '''
    status = True

    # Set the path to save the result in
    save_location = os.path.join(settings.STORAGE_LOCATION, str(search.search_id))
    zip_name = settings.APP_NAME.replace(" ", "") + "_" + search.date_submitted.strftime("%Y%m%d_%H%M%S") + ".zip"
    full_zip_path = os.path.join(save_location, zip_name)

    # Determine file name
    extension = ".json" if search.output_format == "json" else ".csv"
    internal_file_name = f"{search.date_submitted.strftime('%Y%m%d_%H%M%S')}{extension}"
    file_name = f"{search.search_id}{extension}"
    file_path = os.path.join(save_location, file_name)

    # Move the file into the zip
    try:
        with zipfile.ZipFile(full_zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
            zipf.write(file_path, internal_file_name)
        logging.debug(f"(Saver) Added {file_name} to {full_zip_path} for search {search.search_id}")
    except Exception as ex:
        log_error(f"Failed to add {file_path} to {full_zip_path} for search {search.search_id}. {traceback.format_exc()}")
        status = False

    # Remove uncompressed file
    # But don't fail if there is a failure; just email admin a notification
    if status and os.path.exists(file_path):
        try:
           os.remove(file_path)
        except OSError as ose:
            log_error(f"WARNING: Failed to remove uncompressed result for search {search.search_id} at {file_path}")

    return status

def update_search(search, saved, total, retry_count = 0):
    '''
    Update the given search with the newly saved result count
    args:
        search(Search): record to update
        saved(int): number of newly saved results
        total(int): total number of results in the search
        retry_count(int): the number of times this function has failed already
    return:
        (bool): if the function was successful or not
    '''
    status = True

    try:
        # update the search in the database
        search.skip_value = search.skip_value + settings.LN_DOWNLOAD_PER_CALL
        search.update_date = datetime.now()
        search.num_results_in_search = total
        search.num_results_downloaded = search.num_results_downloaded + saved
        search.last_save_dir_size = search.last_save_dir_size + saved
        search.last_save_dir = "1" if not search.last_save_dir else search.last_save_dir

        # check if the search is complete
        if search.num_results_downloaded >= search.num_results_in_search:
            logging.info(f"(Saver) Completed downloading all results for search: {search.search_id}")
            search.date_completed = datetime.now()
            remove_next_search_from_hold(search.userid)

        # save the search record
        search.save(
            update_fields= ["skip_value", "update_date", "num_results_in_search", "num_results_downloaded", \
                    "date_completed", "last_save_dir", "last_save_dir_size"])
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            status = update_search(search, saved, total, retry_count + 1)
        else:
            log_error(f"Failed to update search {search.search_id}. {traceback.format_exc()}")
            status = False
    return status

def remove_next_search_from_hold(userid, retry_count = 0):
    '''
    Update the user's next search as not on hold
    args:
        userid(str): un-mark the user's next search from the hold queue
        retry_count(int): the number of times this function has failed already
    '''
    try:
        searches = apps.get_model('textassembler_web', 'searches')
        next_search = searches.objects.filter(date_completed__isnull=True, failed_date__isnull=True,
             deleted=False, on_hold_date__isnull=False, userid=userid).order_by('update_date').first()
        if next_search:
            logging.info(f"(Saver) Removing search ID {next_search.search_id} from the hold queue.")
            next_search.on_hold_date = None
            next_search.save(update_fields=["on_hold_date"])
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            remove_next_search_from_hold(userid, retry_count + 1)
        else:
            # Not terminating the processor if this happens
            log_error(f"Could not remove the next search for user '{userid}'. {traceback.format_exc()}")
