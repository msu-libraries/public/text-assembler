'''
Prepares record data for saving by converting it to it's
specified format.
'''
import time
import json
import traceback
import logging
import os
from bs4 import BeautifulSoup # pylint: disable=import-error
from textassembler_web.utilities import log_error

def prepare_for_csv(record):
    '''
    Given the full record from the API response, it will convert it to a
    CSV record
    args:
        record (dict): json response from the 'value' element of the records
    return:
        (str): CSV string representation of the record information
    '''
    return {
        "ResultId": record["ResultId"],
        "Title": record["Title"],
        "Overview": record["Overview"],
        "Date": record["Date"],
        "Byline": record["Byline"],
        "WordLength": record["WordLength"],
        "Keyword": record["Keyword"],
        "WebNewsUrl": record["WebNewsUrl"],
        "Section": record["Section"],
        "Jurisdiction": record["Jurisdiction"],
        "InternationalLocation": record["InternationalLocation"],
        "LEI": record["LEI"][0] if record["LEI"] else "",
        "CompanyName": record["CompanyName"][0] if record["CompanyName"] else "",
        "LNGI": record["LNGI"][0] if record["LNGI"] else "",
        "SearchType": record["SearchType"],
        "IsCitationMatch": record["IsCitationMatch"],
        "SourceName": record["Source"]["Name"] if record["Source"] else "",
        "SourceContentType": record["Source"]["ContentType"] if record["Source"] else "",
        "RawFullText": prepare_for_xhtml(record),
        "CleanFullText": prepare_for_text(record)
    }

def prepare_for_json(record):
    '''
    Given the full record from the API response, it will do any
    pre-processing required
    args:
        record (dict): json response from the 'value' element of the records
    return:
        (str): String representation of the JSON data
    '''
    return json.dumps(record, indent=4)

def prepare_for_xhtml(record):
    '''
    Given the full record, return only the portion of the response
    that has the full-text xhtml
    args:
        record (dict): json response from the 'value' element of the records
    return:
        (str): String representation of the 'Document' element
    '''
    content = None

    if "Document" in record and (record["Document"] is None or "Content" not in record["Document"]):
        if "Extracts" in record and record["Extracts"] is not None and "Extract" in record["Extracts"]:
            content = record["Extracts"]["Extract"]
    elif "Document" in record:
        content = record["Document"]["Content"]
    elif "Extracts" in record and len(record["Extracts"]) > 0 and "SummaryText" in record["Extracts"][0]:
        content = record["Extracts"][0]["SummaryText"]

    return content

def prepare_for_text(record):
    '''
    Given the full record, parse the document contents and remove all of the tags
    leaving only the content between the tags
    args:
        record (dict): json response from the 'value' element of the records
    return:
        (str): String representation of the document without xhtml elements
    NOTE:
        If an error occurs stripping the xhtml elements, it will return the
        original data instead to avoid data loss
    '''
    content = None
    output = []

    try:
        bsp = BeautifulSoup(prepare_for_xhtml(record), "html.parser")

        headline = bsp.h1.string if bsp.h1 is not None and bsp.h1.string is not None else ""
        # check another place for headline
        if not headline:
            headline = bsp.find('nitf:hedline').text if bsp.find('nitf:hedline') is not None else ""

        title = bsp.title.string if bsp.title is not None and bsp.title.string is not None else ""

        # write the title and headline
        output.append(title)
        if title != headline: # prevent duplicate output to file
            output.append(headline)

        text = bsp.find('nitf:body').text if bsp.find('nitf:body') is not None else ""
        output.append(text)
        content = "\n\n".join(output)
    except Exception as exp: # pylint: disable=broad-except
        if record:
            logging.warning(f"Unable to create text output. {record}. {traceback.format_exc()}")
        content = prepare_for_json(record)

    return content

