'''
Determine if there is any processing actively running
'''

import sys
import datetime
from django.core.management.base import BaseCommand
from django.apps import apps
from django.conf import settings
from django.db.models import Q
from textassembler_web.ln_api import LNAPI

class Command(BaseCommand):
    '''
    Determine if there is any processing running
    '''
    help = "Determine if any processing is occuring"

    def __init__(self):
        super().__init__()

    def handle(self, *args, **options):
        '''
        Handle the command when run from the command line.
        '''
        api = LNAPI()
        searches = apps.get_model('textassembler_web', 'searches')

        # Check we are in the queue processing window and there are items in the queue
        avail_time = api.check_when_available('download')
        queue = searches.objects.filter(date_completed__isnull=True, failed_date__isnull=True,
                                        deleted=False, on_hold_date__isnull=True)
        if avail_time <= datetime.datetime.now() and queue:
            sys.exit(1)

        # Check if the compression processor has items to be processed
        queue = searches.objects.filter(date_completed__isnull=False, failed_date__isnull=True,
                                        deleted=False, user_notified=False)
        if queue:
            sys.exit(1)

        # Check if the deletion processor has items to be processed
        delete_date = datetime.datetime.now() -  datetime.timedelta(days=settings.NUM_MONTHS_KEEP_SEARCHES * 30)
        queue = searches.objects.filter(
            Q(date_completed__lte=delete_date)|Q(failed_date__lte=delete_date)|Q(deleted=True))
        if queue:
            sys.exit(1)

        # Nothing is running
        sys.exit(0)
