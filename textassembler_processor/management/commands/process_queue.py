"""
Processes the search queue to download results from LexisNexis
"""
import time
import logging
import signal
import traceback
from multiprocessing import Process, Queue, Lock, Event
from queue import Empty
from django.core.management.base import BaseCommand
from django import db
from django.conf import settings
from textassembler_web.utilities import log_error
from textassembler_processor.process_utilities import wait_for_download, get_next_search
from textassembler_processor.process_utilities import get_next_search_to_finalize, set_start_time
from textassembler_processor.process_utilities import sync_skip_value, sync_all_skip_values
from textassembler_processor.process_utilities import get_search_results, update_run_time, save_results
from textassembler_processor.process_utilities import update_search, finalize_search, reset_over_max_skip_values
from textassembler_processor.process_utilities import update_retriever_skip_value, get_search_by_id
from textassembler_processor.delete_utilities import delete_search_files, delete_search_record, prune_logs
from textassembler_processor.delete_utilities import get_next_search_to_delete

terminate_event = Event()
queue = Queue(settings.MAX_SAVER_QUEUE_SIZE)
lock = Lock()

class Command(BaseCommand): # pylint: disable=too-many-instance-attributes
    '''
    Process the search queue downloading results from LexisNexis
    '''
    help = "Process the search queue downloading results from LexisNexis"

    def handle(self, *args, **options):
        '''
        Handles the command to process the queue
        '''
        signal.signal(signal.SIGINT, self.sig_term)
        signal.signal(signal.SIGTERM, self.sig_term)

        logging.info("Starting queue processing.")
        db.connections.close_all() # close all connections forcing each process to re-connect

        # Sync all skip counts
        status = sync_all_skip_values()
        if not status:
            logging.error("Unable to sync all skip counts. Stopping.")
            exit(1)
        status = reset_over_max_skip_values()
        if not status:
            logging.error("Unable to reset invalid skip counts. Stopping.")
            exit(1)

        retriever_proc = Process(target=retriever)
        saver_proc = Process(target=saver)
        finalizer_proc = Process(target=finalizer)
        deleter_proc = Process(target=deleter)
        retriever_proc.daemon = True
        saver_proc.daemon = True
        finalizer_proc.daemon = True
        deleter_proc.daemon = True

        retriever_proc.start()
        saver_proc.start()
        finalizer_proc.start()
        deleter_proc.start()

        while not terminate_event.is_set():
            time.sleep(10) # check every 10 seconds if the processing should be stopped

        logging.info("Attempting to stop processes...")
        # Only give them 10 seconds to wrap up before we kill it
        retriever_proc.join(10)
        time.sleep(1) # give it a second so we don't loose anything in the queue
        saver_proc.join(15) # give it a tad longer so it can finish saving the current batch
        finalizer_proc.join(10) # let it finish compressing it's current file
        deleter_proc.join(5)
        time.sleep(1) # wait before doing is_alive check

        # If anything is still running, log an error and stop
        if retriever_proc.is_alive() or saver_proc.is_alive():
            log_error(
                f"Process did not cleanly stop. Retriever is alive? {retriever_proc.is_alive()}. \
                Saver is alive? {saver_proc.is_alive()}. Finalizer is alive? {finalizer_proc.is_alive()}"
            )

        # If there is anything left over in the queue, update it's record in the DB
        if not queue.empty():
            logging.info("There are items in the results queue that haven't been saved. Syncing DB record.")
        while not queue.empty():
            (search_id, results) = queue.get()
            sync_skip_value(get_search_by_id(search_id))

        logging.info("Stopped queue processing.")

    def sig_term(self, _, __):
        '''
        Handles command termination
        '''
        terminate_event.set()

def retriever():
    '''
    Process in charge of querying LexisNexis' API and adding the results to the queue
    '''
    status = True
    search = None

    logging.debug("(Retriever) Started")

    while not terminate_event.is_set() and status:
        time.sleep(.5)
        db.connections.close_all() # close all connections forcing each process to re-connect

        try: # Put all logic in a try-except so failures don't stop all processes

            # Make sure there is a search in the queue
            search = get_next_search()
            if search == 0:
                time.sleep(5)
                continue # nothing in the queue

            # Wait until there is space in the saver's queue
            if queue.full():
                time.sleep(5)
                continue

            # Wait until the API is available
            status = wait_for_download(terminate_event)
            if not status or terminate_event.is_set():
                continue

            # Get the next search in the queue
            search = get_next_search()
            if search == 0:
                time.sleep(5)
                continue # nothing in the queue
            if not search:
                continue

            # Set the start time of the search
            with lock:
                status = set_start_time(search)
                if not status:
                    continue


            # Get the next set of results for the search
            start_time = time.time()
            results = get_search_results(search)
            if not results:
                # Try one more time but with extract
                results = get_search_results(search, True)
                if not results:
                    continue

            # Update the run time of the search
            with lock:
                status = update_run_time(search, start_time)
                if not status:
                    continue

            # Increment the number of results retrieved
            with lock:
                status = update_retriever_skip_value(search)
                if not status:
                    continue

            # Add the results to the processing queue
            logging.info(f"(Retriever) Adding results (skip:{search.skip_value}|{search.skip_value_retriever}) to the queue for search: {search.search_id}.")
            queue.put((search.search_id, results))
        except Exception as exc:
            log_error(f"(Retriever) Unexpected failure. {traceback.format_exc()}", search)
            status = False


    # Set the terminate flag in case the status was false
    terminate_event.set()


def saver():
    '''
    Process in charge of retrieving results from the queue and saving them to the server
    '''
    status = True
    search = None

    logging.debug("(Saver) Started")

    while not terminate_event.is_set() and status:
        time.sleep(.5)
        db.connections.close_all() # close all connections forcing each process to re-connect

        try: # Put all logic in a try-except so failures don't stop all processes
            search = None
            time.sleep(2)

            # Check for results to process in the queue
            try:
                (search_id, results) = queue.get(block=True, timeout=5)
            except Empty:
                continue # nothing in the queue

            # Get the latest copy of the record
            with lock:
                search = get_search_by_id(search_id)

            # Don't process if unable to get the latest copy of the record
            if not search:
                # put it back in the queue to process later
                queue.put((search_id, results))
                continue

            # Save the results to the server
            logging.info(f"(Saver) Started saving next {settings.LN_DOWNLOAD_PER_CALL} (skip:{search.skip_value}|{search.skip_value_retriever}) results for search: {search.search_id}")
            (status, saved, total) = save_results(search, results)
            if not status:
                continue

            # Update the search's status in the database
            with lock:
                status = update_search(search, saved, total)
                if not status:
                    continue
            logging.info((f"(Saver) Finished saving next {settings.LN_DOWNLOAD_PER_CALL} results "
                f"({search.num_results_downloaded}/{search.num_results_in_search}) for search: {search.search_id}. "
                f"Saver queue size: {queue.qsize()}."))

            # If that search is now complete, compress results and notify the user
            if search.date_completed is not None:
                status = finalize_search(search)
                if not status:
                    continue
                logging.info(f"(Saver) Completed search {search.search_id} and notfied user '{search.userid}'")

        except Exception as exc:
            log_error(f"(Saver) Unexpected failure. {traceback.format_exc()}", search)
            if search:
                status = sync_skip_value(search)
                if status:
                    # We can't just store what files were created because combined searches update a single file
                    # which we wouldn't want to go and figure out what to remove from them.
                    # And it also wouldn't account for failures that just set the status to False
                    log_error((f"ALERT! Failure happened after results were pulled from the queue for search {search.search_id}. "
                        f"There may be lingering files saved before the database could have be updated."))
                else:
                    log_error((f"ALERT! Failure happened after results were pulled from the queue for search {search.search_id}. "
                        f"You will need to manually correct the skip_value field(s) in the database "
                        f"and any lingering files saved before the database could be updated."))
            status = False

    # Set terminate flag in case the status was false
    terminate_event.set()

def finalizer():
    '''
    Process in charge of finalizing results from the queue and notifying users
    '''
    status = True
    search = None

    logging.debug("(Finalizer) Started")

    while not terminate_event.is_set() and status:
        time.sleep(.5)
        db.connections.close_all() # close all connections forcing each process to re-connect

        try: # Put all logic in a try-except so failures don't stop all processes
            search = None
            time.sleep(2)

            # Get the next search in the queue
            search = get_next_search_to_finalize()
            if search == 0:
                time.sleep(5)
                continue # nothing in the queue
            if not search:
                continue

            # Finalize the results
            logging.info(f"(Finalizer) Started finalizing results for search: {search.search_id}")
            status = finalize_search(search)
            if not status:
                continue
            logging.info(f"(Finalizer) Completed search {search.search_id} and notfied user '{search.userid}'")

        except Exception as exc:
            log_error(f"(Finalizer) Unexpected failurer. {traceback.format_exc()}", search)
            status = False

    # Set terminate flag in case the status was false
    terminate_event.set()

def deleter():
    '''
    Process in charge of deleting old results
    '''
    status = True
    search = None

    logging.debug("(Deleter) Started")

    while not terminate_event.is_set() and status:
        time.sleep(.5)
        db.connections.close_all() # close all connections forcing each process to re-connect

        try: # Put all logic in a try-except so failures don't stop all processes
            search = None

            # Prune the logs
            prune_logs()

            # Get the next search in the queue
            search = get_next_search_to_delete()
            if search == 0:
                time.sleep(5)
                continue # nothing in the queue
            if not search: # failed to get the search for some reason
                continue

            logging.info(f"(Deleter) Started removing results for search: {search.search_id}")
            search_id = search.search_id

            # Remmove the results from the file system
            status = delete_search_files(search)
            if not search:
                continue

            # Remove the search from the database
            status = delete_search_record(search)
            if not search:
                continue

            logging.info(f"(Deleter) Completed removing search {search_id}")

        except Exception as exc:
            log_error(f"(Deleter) Unexpected failurer. {traceback.format_exc()}", search)
            status = False

    # Set terminate flag in case the status was false
    terminate_event.set()
