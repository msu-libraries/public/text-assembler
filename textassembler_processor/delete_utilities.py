import traceback
import os
import time
import logging
import shutil
from datetime import datetime, timedelta
from django.conf import settings
from django.db.utils import OperationalError
from django.db.models import F, Q
from django.apps import apps
from textassembler_web.utilities import log_error
from textassembler_processor.models import api_log


def get_next_search_to_delete():
    '''
    Get the next search to delete from the database
        retry_count(int): Used to track the number of failures we've had already
    returns:
        (Search/None): database record for the next search to process, will
            be None if there is nothing to search
    '''
    search = None
    try:
        # check that there are items in the queue to process
        searches = apps.get_model('textassembler_web', 'searches')
        delete_date = datetime.now() -  timedelta(days=settings.NUM_MONTHS_KEEP_SEARCHES * 30)
        queue = searches.objects.filter(Q(date_completed__lte=delete_date) | Q(deleted=True)).order_by('update_date')
        if queue:
            search = queue[0]
        else:
            search = 0
    except Exception as ex:
        if "Can't connect to server" in str(ex):
            logging.warning(f"(Deleter) Failed to get the next search. {traceback.format_exc()}")
        else:
            log_error(f"(Deleter) Failed to get the next search. {traceback.format_exc()}")
    return search

def delete_search_files(search, retry_count = 0):
    '''
    Delete search files
    args:
        search(Search): Search to remove results for
        retry_count(int): Used to track the number of failures we've had already
    returns:
        (bool): Sucess or failure of the function
    '''
    status = True

    try:
        save_location = os.path.join(settings.STORAGE_LOCATION, str(search.search_id))
        zip_name = settings.APP_NAME.replace(" ", "") + "_" + search.date_submitted.strftime("%Y%m%d_%H%M%S") + ".zip"
        zip_path = os.path.join(save_location, zip_name)

        if os.path.isdir(save_location):
            try:
                shutil.rmtree(save_location)
            except OSError as ex1:
                log_error(f"(Deleter) Could not delete files for search {search.search_id}. {ex1}", search)
        if zip_path != None and os.path.exists(zip_path):
            try:
                os.remove(zip_path)
            except OSError as ex2:
                log_error(f"(Deleter) Could not delete the zipped file for search {search.search_id}. {ex2}", search)
        if os.path.isdir(save_location):
            try:
                os.rmdir(save_location)
            except OSError as ex4:
                log_error(f"(Deleter) Could not delete root directory for search {search.search_id}. {ex4}", search)
    except Exception as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            search = delete_search_files(search, retry_count + 1)
        else:
            log_error(f"(Deleter) failed to delete_search_files (search id: {search.search_id}) \
                after {settings.NUM_RETRIES} attempts. {traceback.format_exc()}")
            status = False

    return status

def delete_search_record(search, retry_count = 0):
    '''
    Delete search in the database
    args:
        search(Search): Search to remove from database
        retry_count(int): Used to track the number of failures we've had already
    returns:
        (bool): Sucess or failure of the function
    '''
    status = True

    try:
        search.refresh_from_db()
        search.delete()
    except OperationalError as ex:
        if retry_count <= settings.NUM_RETRIES:
            logging.error(traceback.format_exc())
            time.sleep(30)
            search = delete_search_record(search, retry_count + 1)
        else:
            log_error(f"(Deleter) failed to delete_search_record (search id: {search.search_id}) \
                after {settings.NUM_RETRIES} attempts. {traceback.format_exc()}")
            status = False

    return status

def prune_logs():
    '''
    Prune logs from the database so they don't build up
    '''
    try:
        # delete logs older than the number of months we retain searches
        delete_date = datetime.now() -  timedelta(days=settings.NUM_MONTHS_KEEP_SEARCHES * 30)
        api_log.objects.filter(request_date__lte=delete_date).delete()
    except OperationalError as ex:
        if "Can't connect to server" in str(ex):
            logging.warning(f"(Deleter) WARNING: Failed to prune the database logs. {ex}")
        else:
            log_error(f"(Deleter) WARNING: Failed to prune the database logs. {ex}")
