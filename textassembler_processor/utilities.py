'''
Prepares record data for saving by converting it to it's
specified format.
'''
import time
import json
import traceback
import os
from bs4 import BeautifulSoup # pylint: disable=import-error
from textassembler_web.utilities import log_error, create_error_message

def prepare_for_csv(record):
    '''
    Given the full record from the API response, it will convert it to a
    CSV record
    args:
        record (dict): json response from the 'value' element of the records
    return:
        (str): CSV string representation of the record information
    '''
    return {
        "ResultId": record["ResultId"],
        "Title": record["Title"],
        "Overview": record["Overview"],
        "Date": record["Date"],
        "Byline": record["Byline"],
        "WordLength": record["WordLength"],
        "Keyword": record["Keyword"],
        "WebNewsUrl": record["WebNewsUrl"],
        "Section": record["Section"],
        "Jurisdiction": record["Jurisdiction"],
        "InternationalLocation": record["InternationalLocation"],
        "LEI": record["LEI"][0] if record["LEI"] else "",
        "CompanyName": record["CompanyName"][0] if record["CompanyName"] else "",
        "LNGI": record["LNGI"][0] if record["LNGI"] else "",
        "SearchType": record["SearchType"],
        "IsCitationMatch": record["IsCitationMatch"],
        "SourceName": record["Source"]["Name"] if record["Source"] else "",
        "SourceContentType": record["Source"]["ContentType"] if record["Source"] else ""
    }

def prepare_for_json(record):
    '''
    Given the full record from the API response, it will do any
    pre-processing required
    args:
        record (dict): json response from the 'value' element of the records
    return:
        (str): String representation of the JSON data
    '''
    return json.dumps(record, indent=4)

def prepare_for_xhtml(record):
    '''
    Given the full record, return only the portion of the response
    that has the full-text xhtml
    args:
        record (dict): json response from the 'value' element of the records
    return:
        (str): String representation of the 'Document' element
    '''
    content = None

    if record["Document"] is None or "Content" not in record["Document"]:
        if "Extracts" in record and record["Extracts"] is not None and "Extract" in record["Extracts"]:
            content = record["Extracts"]["Extract"]
    else:
        content = record["Document"]["Content"]

    return content

def prepare_for_text(record):
    '''
    Given the full record, parse the document contents and remove all of the tags
    leaving only the content between the tags
    args:
        record (dict): json response from the 'value' element of the records
    return:
        (str): String representation of the document without xhtml elements
    NOTE:
        If an error occurs stripping the xhtml elements, it will return the
        original data instead to avoid data loss
    '''
    content = None
    output = []

    try:
        bsp = BeautifulSoup(record["Document"]["Content"], "html.parser")

        headline = bsp.h1.string if bsp.h1 is not None and bsp.h1.string is not None else ""
        # check another place for headline
        if not headline:
            headline = bsp.find('nitf:hedline').text if bsp.find('nitf:hedline') is not None else ""

        title = bsp.title.string if bsp.title is not None and bsp.title.string is not None else ""

        # write the title and headline
        output.append(title)
        if title != headline: # prevent duplicate output to file
            output.append(headline)

        text = bsp.find('nitf:body').text if bsp.find('nitf:body') is not None else ""
        output.append(text)
        content = "\n\n".join(output)
    except Exception as exp: # pylint: disable=broad-except
        log_error((f"Unable to create text output, "
                   f"{create_error_message(exp, os.path.basename(__file__))}"))
        content = prepare_for_json(record)

    return content

def handle_error(retry_count, retry_max, sleep_time):
    '''
    args:
        retry_count (int): Current number of retries
        retry_max (int): Max number of retries before terminating
        sleep_time (int): How long to wait before returning
    returns:
        (bool, bool): If the application should terminate and it's new
            retry count
    '''
    terminate = False
    if retry_counts <= retry_max:
        time.sleep(sleep_time)
        retry_count = retry_count + 1
    else:
        log_error("Stopping queue processor. {traceback.format_exc()}")
        terminate = True
    return (terminate, retry_count)

def remove_files(files_to_remove=None, message=""):
    '''
    Removes the list of files from the filesystem
    Params:
        files_to_remove (array): list of full filepaths to delete
        message (string): message to print before removing files
    '''
    if not files_to_remove:
        return # nothing to remove

    logging.warning(f"Removing {len(files_to_remove)} from the server. {message}")
    for file_to_remove in files_to_remove:
        if os.path.isfile(file_to_remove):
            logging.warning(f"Deleting {file_to_remove}.")
            try:
                os.remove(file_to_remove)
            except OSError as ose:
                log_error(f"Failed to delete {file_to_remove} from the server. {create_error_message(ose, os.path.basename(__file__))}")
