Quick Start Guide
-----------------
Use this guide to quickly get a local instance of the site up and running for evaluation purposes. These steps were
written to work on Ubuntu (I use Ubuntu 20.04), but hopefully should be similar on other Linux based systems.

* Install Docker
```
apt update
apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io
```

* Initialize the Swam
```
# Where <IP> is the current machine's IP
docker swarm init --advertise-addr <IP> --listen-addr <IP>:2377
```

* Create the volume directories (if you want to change the paths, just change them in the `docker-compose` files)
```
mkdir /mnt/ta_db
mkdir /mnt/ta_db_logs
mkdir /mnt/textassembler
```

* Setup main config file, replace at least these variables, but open it to skim for other 
settings you might want to change to
```
CLIENT_ID=[API client id provided by LexisNexis]
CLIENT_SECRET=[API secret provided by LexisNexis]
RELAY_HOST=myrelayhost@mail.example.edu
MAINTAINER_EMAILS=myemail@example.edu
EMAIL_FROM=root@localhost

cp textassembler.cfg.example textassembler.cfg
sed -i "s/_BYPASS_/TRUE/g" textassembler.cfg
sed -i "s/_CLIENT_ID_/$CLIENT_ID/g" textassembler.cfg
sed -i "s/_CLIENT_SECRET_/$CLIENT_SECRET/g" textassembler.cfg
sed -i "s/_MAINTAINER_EMAILS_/$MAINTAINER_EMAILS/g" textassembler.cfg
sed -i "s/_EMAIL_FROM_/$EMAIL_FROM/g" textassembler.cfg

# Setup email so notifications can go to users
cp main.cf.example main.cf
sed -i "s/_HOSTNAME_/textassembler.test.lib.msu.edu/g" main.cf
sed -i "s/_RELAY_HOST_/$RELAY_HOST/g" main.cf
```

* Now you're ready to build the image
```
docker build . -t ta:latest
```

* Now let's start it up!
```
docker stack deploy -c docker-compose.yml -c docker-compose.local.yml -c docker-compose.processors.yml ta
```

* Run a one-time API limit update to populate the limits database table for the first time
```
cat db/01_populate_api_limits.sql | docker exec -i $(docker ps -q -f name=ta_db) mysql -p123456 textassembler
docker exec $(docker ps -q -f name=ta_web) python manage.py update_limits
```

* Now go to your browser and make sure the site works and you can run a test search  
http://0.0.0.0:8000/search

Other helpful commands:
* To see the logs from the services
```
docker service logs ta_web -f
docker service logs ta_queue -f
```
* To stop the stack
```
docker stack rm ta
```
