INSERT INTO textassembler_web_api_limits (limit_type, limit_per_minute, limit_per_hour, limit_per_day, update_date, 
    remaining_per_minute, remaining_per_hour, remaining_per_day,
    reset_on_minute, reset_on_hour, reset_on_day) VALUES
('CallTypeChoice.DWL', 5, 200, 1000, NOW(),
    5,200,1000, NOW(), NOW(), NOW())
ON DUPLICATE KEY UPDATE limit_type = limit_type;
INSERT INTO textassembler_web_api_limits (limit_type, limit_per_minute, limit_per_hour, limit_per_day, update_date,
    remaining_per_minute, remaining_per_hour, remaining_per_day,
    reset_on_minute, reset_on_hour, reset_on_day) VALUES
('CallTypeChoice.SRC', 5, 100, 1000, NOW(),
    5,100,1000, NOW(), NOW(), NOW())
ON DUPLICATE KEY UPDATE limit_type = limit_type;
INSERT INTO textassembler_web_api_limits (limit_type, limit_per_minute, limit_per_hour, limit_per_day, update_date,
    remaining_per_minute, remaining_per_hour, remaining_per_day,
    reset_on_minute, reset_on_hour, reset_on_day) VALUES
('CallTypeChoice.SRH', 125, 1500, 12000, NOW(),
    125,1500,12000, NOW(), NOW(), NOW())
ON DUPLICATE KEY UPDATE limit_type = limit_type;
